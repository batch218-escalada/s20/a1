// Part 1

let number = Number(prompt('Give me a number:'));

console.log('The number you provided is ' + number);

for (let i = number; i >= 0 && i >=50; i--) {
    if (i <= 50) {
        console.log('The current value is at ' + i + '.' + ' Terminating the loop.');
    }
    else if ((i%10) === 0) {
        console.log('The number is divisible by 10. Skipping the number.');
    }
    else if ((i%5) === 0) {
        console.log(i);
    }
}

// Part 2

let myWord = 'supercalifragilisticexpialidocious';
console.log(myWord);

let consonants = '';

for (let i = 0; i < myWord.length; i++) {
    if (
        myWord[i].toLowerCase() == 'a' ||
        myWord[i].toLowerCase() == 'e' ||
        myWord[i].toLowerCase() == 'i' ||
        myWord[i].toLowerCase() == 'o' ||
        myWord[i].toLowerCase() == 'u' 
    ) {
    }
    else {
        consonants += myWord[i];
    }
}

console.log(consonants);